login:
	pipenv run python src/sws_login.py

logout:
	pipenv run python src/sws_logout.py

service-list:
	pipenv run python src/sws_service_list.py | column --table --output-separator="|"
