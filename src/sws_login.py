import requests
import getpass
import os
from pathlib import Path
import config
from common import sws_token_exists, sws_get_local_token

def sws_get_token(username, password):
    '''
    Make API call to SWS and get the token
    '''
    _req = requests.post(config.token_url,
                         headers={"Content-Type": "application/json"},
                         json={"username": username, "password": password})
    _error = _req.json().get("non_field_errors")
    if _req.status_code == 200: # success
        token = _req.json().get("token", None)
        if token is None:
            return False, _error[0]
        return True, token
    return False, _error[0]

def sws_store_token(token):
    '''
    Write the token into ~/.local/sws/token
    '''
    
    token_filename = config.token_filename
    if not os.path.exists(os.path.dirname(token_filename)):
        try:
            os.makedirs(os.path.dirname(token_filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    with open(token_filename, "w") as f:
        f.write(token)


def sws_token(username, password):
    '''
    Wrapper over sws_get_token and sws_store token
    Gets token from username, password and then
    stores the token into the file
    '''
    if sws_token_exists():
        token = sws_get_local_token()
    else:
        token = sws_get_token(username, password)
    if token[0]:
        sws_store_token(token[1])
        return True, token[1]
    else:
        return False, token[1]


def sws_login():
    if sws_token_exists():
        return sws_get_local_token()
    '''
    Get the username and password.
    The default username is the login name.
    '''

    default_username = getpass.getuser()
    username = input(f'Username [{default_username}]: ')
    password = getpass.getpass()

    if not username:
        username = default_username
    return sws_token(username, password)
        
def sws_headers():
    if sws_token_exists():
        token = sws_get_local_token()[1]
        return {
            "Content-Type": "application/json",
            "Authorization": f'Token {token}'
        }
    else:
        if sws_login()[0]:
            return sws_headers()

if __name__ == '__main__':
    login = sws_login()
    if login[0]:
        print("Login Successful!")
    else:
        print("Login Failed!", login[1])
        
