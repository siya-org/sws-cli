import config
import os

def sws_token_exists():
    return os.path.exists(config.token_filename)

def sws_get_local_token():
    with open(config.token_filename) as f:
        return True, f.read()


