import requests
from sws_login import sws_headers
import config
import json

def sws_service_data():
    headers = sws_headers()
    req = requests.get(f"{config.service_url}", headers=headers)
    if req.status_code == 200: # success
        return req.json()
    return None

def sws_service_list():
    data = sws_service_data()
    if data is None:
        return
    services = [[_['id'], _['name']] for _ in data['results']]
    print("ID\tName")
    for service in services:
        print(*service)


if __name__ == '__main__':
    sws_service_list()
