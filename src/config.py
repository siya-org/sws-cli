from pathlib import Path

token_filename = '~/.local/sws/token'
token_filename = Path(token_filename).expanduser()


base_url = "https://siyasang.com"
token_url = base_url + "/token/"
service_url = base_url + "/service/s"
