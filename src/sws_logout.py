import os
import config
from common import sws_token_exists

def sws_delete_token():
    if sws_token_exists():
        os.remove(config.token_filename)
        return True
    return False

def sws_logout():
    if sws_delete_token():
        print("Successfully logged out!")
    else:
        print("Already logged out")


if __name__ == '__main__':
    sws_logout()
